package fr.iut.ouaff.data.chargeur

import fr.iut.ouaff.modele.metier.Chien

interface Chargeur {

    public fun charger(nomFichier : String) : MutableList<Chien>
}
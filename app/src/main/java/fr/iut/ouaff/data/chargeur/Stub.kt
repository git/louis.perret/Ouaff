package fr.iut.ouaff.data.chargeur

import fr.iut.ouaff.modele.metier.Chien
import fr.iut.ouaff.modele.metier.Genre

class Stub : Chargeur {
    override fun charger(nomFichier: String): MutableList<Chien> {
        var listeChien: MutableList<Chien> = ArrayList()
        listeChien.add(Chien("Louis", "Golden Retriever", Genre.Male, 40, 0))
        listeChien.add(Chien("Elfe", "Dalmatien", Genre.Inconnu, 30, 1))
        listeChien.add(Chien("Dexter", "Pitbull allemand", Genre.Male, 50, 3))
        listeChien.add(Chien("Daisy", "Chihuahua", Genre.Femele, 20, 2))
        listeChien.add(Chien("Deku", "Labrador", Genre.Femele, 35,1))

        listeChien.add(Chien("Louis", "Golden Retriever", Genre.Male, 40, 0))
        listeChien.add(Chien("Elfe", "Dalmatien", Genre.Inconnu, 30, 1))
        listeChien.add(Chien("Dexter", "Pitbull allemand", Genre.Male, 50, 3))
        listeChien.add(Chien("Daisy", "Chihuahua", Genre.Femele, 20, 2))
        listeChien.add(Chien("Deku", "Labrador", Genre.Femele, 35,1))
        listeChien.add(Chien("Louis", "Golden Retriever", Genre.Male, 40, 0))
        listeChien.add(Chien("Elfe", "Dalmatien", Genre.Inconnu, 30, 1))
        listeChien.add(Chien("Dexter", "Pitbull allemand", Genre.Male, 50, 3))
        listeChien.add(Chien("Daisy", "Chihuahua", Genre.Femele, 20, 2))
        listeChien.add(Chien("Deku", "Labrador", Genre.Femele, 35,1))

        return listeChien
    }
}
package fr.iut.ouaff.utils

import fr.iut.ouaff.modele.metier.Chien

public interface DogSelectedCallback {

    fun dogSelectedCallback(selectedDog: Chien, position: Int)
}
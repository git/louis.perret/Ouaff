package fr.iut.ouaff.vues.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.SimpleOnItemTouchListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import fr.iut.ouaff.R
import fr.iut.ouaff.modele.metier.Chien
import fr.iut.ouaff.utils.DogSelectedCallback
import fr.iut.ouaff.vues.adapter.AdapterChien

class FragmentMaster(private var listeChiens: MutableList<Chien>, private var listener:DogSelectedCallback) : Fragment() {

    private lateinit var boutonAjouterChien: FloatingActionButton

    private lateinit var adapter: AdapterChien

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_master, container, false);
        val recyclerView = view.findViewById<RecyclerView>(R.id.recyclerViewChien);
        recyclerView.layoutManager = GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
        adapter =  AdapterChien(listeChiens, listener)
        recyclerView.adapter = adapter
        boutonAjouterChien = view.findViewById(R.id.floatingActionButton)
        return view
    }

    public fun getBoutonAjouterChien() = boutonAjouterChien

    public fun listDogsChanged(){
        adapter.notifyDataSetChanged()
    }

}
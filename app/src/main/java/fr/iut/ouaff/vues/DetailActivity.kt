package fr.iut.ouaff.vues

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import fr.iut.ouaff.R
import fr.iut.ouaff.modele.metier.Chien
import fr.iut.ouaff.modele.metier.Genre
import fr.iut.ouaff.vues.fragment.FragmentDetail


class DetailActivity : AppCompatActivity(), FragmentDetail.ListenerDetail {

    private lateinit var fragmentDetail:FragmentDetail

    private lateinit var chienSelected: Chien

    private var position: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        chienSelected = intent?.getSerializableExtra(CHIEN) as Chien
        position = intent?.getIntExtra(POSITION,0)!!

        if(supportFragmentManager.findFragmentById(R.id.id_fragmentDetail) == null){
            fragmentDetail = FragmentDetail(this, chienSelected, position)
            supportFragmentManager.beginTransaction()
                .add(R.id.id_fragmentDetail, fragmentDetail)
                .commit()
        }
    }


    companion object{
        const val CHIEN = "chien"
        const val POSITION = "position"
        const val NOMCHIEN:String = "nomChien"
        const val RACECHIEN:String = "raceChien"
        const val GENRECHIEN:String = "genreChien"
        const val MESURECHIEN:String = "mesureChien"
        const val AGRESSIVITECHIEN:String = "agressiviteChien"
        const val AJOUTCHIEN: String = "ajoutChien"

        public fun createIntent(context: Context, chien:Chien, position:Int): Intent = Intent(context, DetailActivity::class.java).apply {
            putExtra(CHIEN, chien)
            putExtra(POSITION, position)
        }

        public fun createIntentAjoutChien(context:Context): Intent = Intent(context, DetailActivity::class.java)

        public fun getNomChien(result:Intent) = result.getStringExtra(NOMCHIEN)
        public fun getRaceChien(result:Intent) = result.getStringExtra(RACECHIEN)
        public fun getGenreChien(result:Intent) = result.getSerializableExtra((GENRECHIEN))
        public fun getMesureChien(result:Intent) = result.getIntExtra(MESURECHIEN, 0)
        public fun getAgressiviteChien(result:Intent) = result.getIntExtra(AGRESSIVITECHIEN, 0)
    }

    override fun ajouterChien(
        nom: String?,
        race: String?,
        genre: Genre,
        poids: Int,
        agressivite: Int
    ) {
        val data = Intent().apply {
            putExtra(AJOUTCHIEN, true)
            putExtra(NOMCHIEN, nom)
            putExtra(RACECHIEN, race)
            putExtra(GENRECHIEN, genre)
            putExtra(MESURECHIEN, poids)
            putExtra(AGRESSIVITECHIEN, agressivite)
        }

        setResult(RESULT_OK, data)
        finish()
    }

    override fun saveDog(pos:Int,
                         nom: String?,
                         race: String?,
                         genre: Genre,
                         poids: Int,
                         agressivite: Int
    ) {
        val data = Intent().apply {
            putExtra(AJOUTCHIEN, false)
            putExtra(POSITION, pos)
            putExtra(NOMCHIEN, nom)
            putExtra(RACECHIEN, race)
            putExtra(GENRECHIEN, genre)
            putExtra(MESURECHIEN, poids)
            putExtra(AGRESSIVITECHIEN, agressivite)
        }

        setResult(RESULT_OK, data)
        finish()
    }
}
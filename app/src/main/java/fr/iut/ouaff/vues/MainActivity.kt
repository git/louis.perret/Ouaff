package fr.iut.ouaff.vues

import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import fr.iut.ouaff.R
import fr.iut.ouaff.data.chargeur.Stub
import fr.iut.ouaff.modele.metier.Chien
import fr.iut.ouaff.modele.metier.Genre
import fr.iut.ouaff.utils.DogSelectedCallback
import fr.iut.ouaff.vues.fragment.FragmentDetail
import fr.iut.ouaff.vues.fragment.FragmentMaster

class MainActivity : AppCompatActivity(), DogSelectedCallback, FragmentDetail.ListenerDetail {

    private var listeChiens: MutableList<Chien> = Stub().charger("")
    private lateinit var fragment_Master:FragmentMaster
    private lateinit var fragment_Detail: FragmentDetail

    private var activityLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){ result ->
        if(result.resultCode == RESULT_OK){
            result.data?.let {
                if(it.getBooleanExtra(DetailActivity.AJOUTCHIEN, false)){
                        ajouterChien(
                            DetailActivity.getNomChien(it),
                            DetailActivity.getRaceChien(it),
                            DetailActivity.getGenreChien(it) as Genre,
                            DetailActivity.getMesureChien(it),
                            DetailActivity.getAgressiviteChien(it))
                }
                else{
                    saveDog(
                        it.getIntExtra(DetailActivity.POSITION,-1),
                        it.getStringExtra(DetailActivity.NOMCHIEN),
                        it.getStringExtra(DetailActivity.RACECHIEN),
                        it.getSerializableExtra(DetailActivity.GENRECHIEN) as Genre,
                        it.getIntExtra(DetailActivity.MESURECHIEN,0),
                        it.getIntExtra(DetailActivity.AGRESSIVITECHIEN,0)
                    )
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        if(supportFragmentManager.findFragmentById(R.id.id_fragment_master) == null){
            fragment_Master = FragmentMaster(listeChiens, this)
            supportFragmentManager.beginTransaction()
                .add(R.id.id_fragment_master, fragment_Master)
                .commit()
        }

        if(resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
            if(supportFragmentManager.findFragmentById(R.id.id_fragment_detail) == null) {
                fragment_Detail = FragmentDetail(this, null, -1)
                supportFragmentManager.beginTransaction()
                    .add(R.id.id_fragment_detail, fragment_Detail)
                    .commit()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            fragment_Master.getBoutonAjouterChien().setOnClickListener() {
                activityLauncher.launch(
                    DetailActivity.createIntentAjoutChien(this)
                )
            }
        }
        else{
            fragment_Master.getBoutonAjouterChien().visibility = View.INVISIBLE
        }
    }

    override fun ajouterChien(nom: String?, race: String?, genre: Genre, poids: Int, agressivite: Int) {
        if(nom != null && race != null) listeChiens.add(Chien(nom, race, genre, poids, agressivite))
        fragment_Master.listDogsChanged()
    }

    override fun dogSelectedCallback(selectedDog: Chien, position: Int) {
        if(resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT){
            activityLauncher.launch(DetailActivity.createIntent(this, selectedDog, position))
        }
        else{
            fragment_Detail.setCurrentDog(selectedDog, position)
        }
    }

    override fun saveDog(pos:Int, nom: String?, race: String?, genre: Genre, poids: Int, agressivite: Int){
        if(pos >= 0){
            val chien = listeChiens.get(pos)
            chien.nom = nom!!
            chien.race = race!!
            chien.genre = genre
            chien.poids = poids
            chien.agressivite = agressivite

            fragment_Master.listDogsChanged()
        }
    }
}
package fr.iut.ouaff.vues.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.RatingBar
import android.widget.Spinner
import androidx.fragment.app.Fragment
import fr.iut.ouaff.R
import fr.iut.ouaff.modele.metier.Chien
import fr.iut.ouaff.modele.metier.Genre

class FragmentDetail(private var listener:ListenerDetail, private var chien: Chien?, private var position:Int) : Fragment() {

    private lateinit var boutonQuitter:ImageButton

    private lateinit var textFieldNomChien:EditText
    private lateinit var textFieldRaceChien:EditText
    private lateinit var spinnerGenre:Spinner
    private lateinit var textFieldMesureChien:EditText
    private lateinit var ratingBarAgressivite:RatingBar

    private val arrayGenre = Genre.values()

    interface ListenerDetail{
        fun ajouterChien(nom: String?, race: String?, genre: Genre, poids: Int, agressivite: Int)
        fun saveDog(pos:Int, nom: String?, race: String?, genre: Genre, poids: Int, agressivite: Int)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_detail
        , container, false)

        with(view) {
            textFieldNomChien = findViewById(R.id.textFieldNomChien)
            textFieldRaceChien = findViewById(R.id.textFieldRaceChien)
            spinnerGenre = findViewById(R.id.spinnerGenre)
            textFieldMesureChien = findViewById(R.id.textFieldMesureChien)
            ratingBarAgressivite = findViewById(R.id.ratingBarAgressivite)
            boutonQuitter = findViewById(R.id.bouttonAjouter)
        }

        spinnerGenre.adapter = ArrayAdapter(view.context, R.layout.cellule_spinner, arrayGenre)
        boutonQuitter.visibility = Button.VISIBLE
        setCurrentDog(chien, position)
        return view
    }

    private fun setDataDog(){
        textFieldNomChien.setText(chien?.nom)
        textFieldRaceChien.setText(chien?.race)
        spinnerGenre.setSelection(arrayGenre.indexOf(chien?.genre))
        textFieldMesureChien.setText(chien?.poids.toString())
        ratingBarAgressivite.rating = chien?.agressivite!!.toFloat()
    }

    fun setCurrentDog(dog:Chien?, position:Int){
        this.position = position
        if(dog == null){
            chien = dog
            textFieldNomChien.setText("")
            textFieldRaceChien.setText("")
            textFieldMesureChien.setText("")
            ratingBarAgressivite.rating = 1F

            boutonQuitter.setOnClickListener() { listener.ajouterChien(
                textFieldNomChien.text.toString(),
                textFieldRaceChien.text.toString(),
                spinnerGenre.selectedItem as Genre,
                textFieldMesureChien.text.toString().toInt(),
                ratingBarAgressivite.rating.toInt())
            }
        }
        else{
            chien = dog
            setDataDog()
            boutonQuitter.setOnClickListener() {
                listener.saveDog(
                    position,
                    textFieldNomChien.text.toString(),
                    textFieldRaceChien.text.toString(),
                    spinnerGenre.selectedItem as Genre,
                    textFieldMesureChien.text.toString().toInt(),
                    ratingBarAgressivite.rating.toInt())
            }
        }
    }
}
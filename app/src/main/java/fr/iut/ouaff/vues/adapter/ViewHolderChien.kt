package fr.iut.ouaff.vues.adapter

import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import fr.iut.ouaff.R
import fr.iut.ouaff.modele.metier.Chien
import fr.iut.ouaff.utils.DogSelectedCallback

/**
 * Affichera un de mes chiens -> équivalent des cellules en javafx
 */
class ViewHolderChien(itemView: View, listener: DogSelectedCallback) : ViewHolder(itemView) {

    lateinit var chien: Chien

    var posChien: Int = 0

    var textNomChien: TextView
        get() = field
        private set

    var textRaceChien: TextView
        get() = field
        private set

    var cardViewChien: CardView
        get()= field
        private set

    /// Initialise mes propriétés
    init {
        textNomChien = itemView.findViewById(R.id.nomChien)
        textRaceChien = itemView.findViewById(R.id.raceChien)
        cardViewChien = itemView.findViewById(R.id.cardViewChien)

        cardViewChien.setOnClickListener(){ chien?.let { listener.dogSelectedCallback(it, posChien)}}
    }

    public fun bindWithDog(chien:Chien, pos:Int){
        this.chien = chien
        this.posChien = pos
        textNomChien.text = chien.nom // afficher le nom
        textRaceChien.text = chien.race

        /// setter la couleur à afficher suivant son aggressivité
        // when équivalent du switch case en Java ou en C#
        cardViewChien.setCardBackgroundColor(
            ContextCompat.getColor(itemView.context,
                when(chien.agressivite){
                    1 -> R.color.colorNormal
                    2 -> R.color.colorBad
                    3 -> R.color.colorAggressive
                    else ->  R.color.colorNice
                }
            )
        )
    }
}
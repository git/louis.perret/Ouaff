package fr.iut.ouaff.vues.adapter

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import fr.iut.ouaff.R
import fr.iut.ouaff.modele.metier.Chien
import fr.iut.ouaff.utils.DogSelectedCallback

/**
 * Gère l'affichage de la liste de mes chiens -> équivalent de la ListView en javafx
 */
class AdapterChien(private var listeChiens:MutableList<Chien>, private var listener: DogSelectedCallback): RecyclerView.Adapter<ViewHolder>() {

    private lateinit var ressources: Resources;

    /**
     * Créer la ViewHolder qui va contenir un chien
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        ressources = parent.resources // pour pouvoir accéder au R. plus tard
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cellule_chien, parent, false) // je charge la vue xml qui contiendra mon chien
        return ViewHolderChien(view, listener) // je passe cette vue à ma ViewHolderChien
    }

    /**
     * On ajoute les données du chien à afficher dans la ViewHolder créée
     *  holder -> représente ma ViewHolderChien
     *  position -> position du chien à afficher dans ma listeChien
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chienAAfficher = listeChiens.get(position) // on récupère le chien à afficher
        if(holder is ViewHolderChien){ // si ma ViewHolder est bien une instance de ViewHolderChien
                holder.bindWithDog(chienAAfficher, position)

            // holder.cardViewChien.setOnClickListener() { }
        }
    }

    /**
     * Renvoie le nombre total d'élément de la liste à afficher
     */
    override fun getItemCount(): Int = listeChiens.size

}
package fr.iut.ouaff.modele.metier

import fr.iut.ouaff.data.chargeur.Stub

class Chenil {

    var listeChiens: MutableList<Chien> = ArrayList()
        get() = listeChiens

    constructor(){
        listeChiens = Stub().charger("")
    }

    public fun ajouterChien(chien: Chien){
        listeChiens.add(chien)
    }

    public fun ajouterChien(nom: String, race: String, genre: Genre, poids: Int, agressivite: Int){
        listeChiens.add(Chien(nom, race, genre, poids, agressivite))
    }
}
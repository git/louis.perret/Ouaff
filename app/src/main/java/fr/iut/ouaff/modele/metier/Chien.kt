package fr.iut.ouaff.modele.metier

class Chien : java.io.Serializable{

    var nom:String
        public get() = field
        set

    var race:String
        public get() = field
        set

    var genre:Genre
        public get() = field
        set

    var poids:Int
        public get() = field
        set(value) {
            if(value > 0) field = value
        }

    var agressivite:Int
        public get() = field
        set(value){
            if(value >= 0 && value <= 3){
                field = value
            }
        }

    constructor(nom: String, race: String, genre: Genre, poids: Int, agressivite: Int) {
        this.nom = nom
        this.race = race
        this.genre = genre
        this.poids = poids
        this.agressivite = agressivite
    }

}